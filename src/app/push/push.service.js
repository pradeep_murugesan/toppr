(function() {
  'use strict';

  angular
    .module('toppr')
    .factory('pushSvc', pushSvc);

  /** @ngInject */
  function pushSvc($log, $http) {
    var api = 'https://hackerearth.0x10.info/api/one-push?type=json&query=push';
    //var api = 'http://jwttester.getsandbox.com/contributors'
    var service = {
      pushWebSite: pushWebSite
    };

    return service;

    function pushWebSite(website) {
      api = api + "&url=" + website.webUrl + "&tag=" + website.tag + "&title=" + website.title;
      $log.info(api);
      return $http.post(api)
        .then(pushWebsiteComplete)
        .catch(pushWebsiteFailed);

      function pushWebsiteComplete(response) {
        return response.data;
      }

      function pushWebsiteFailed(error) {
        $log.error('XHR Failed for get websites.\n' + angular.toJson(error.data, true));
      }
    }
  }
})();
