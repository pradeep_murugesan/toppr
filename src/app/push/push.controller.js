(function() {
  'use strict';

  angular
    .module('toppr')
    .controller('PushController', PushController);

  /** @ngInject */
  function PushController($log, pushSvc, toastr) {
      var vm = this;
      vm.website = {}
      vm.addWebsite = function() {
        pushSvc.pushWebSite(vm.website).then(function (data) {
          vm.response = data;
        }).catch(function (data) {
          $log.error(data);
        });
      }
  }
})();
