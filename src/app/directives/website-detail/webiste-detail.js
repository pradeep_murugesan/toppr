(function() {
  'use strict';

  angular
    .module('toppr')
    .directive('websiteDetail', websiteDetail);

  /** @ngInject */
  function websiteDetail() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/website-detail/website-detail.html',
      scope: {
        website: '=',
        likeWebsite: '&'
      }
    };
    return directive;
  }

})();
