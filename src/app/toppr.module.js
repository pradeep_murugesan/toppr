(function() {
  'use strict';

  angular
    .module('toppr', ['ngAnimate', 'ngResource', 'toastr', 'ngRoute', 'blockUI', 'ngStorage']);

})();
