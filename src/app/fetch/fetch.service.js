(function() {
  'use strict';

  angular
    .module('toppr')
    .factory('fetchSvc', fetchSvc);

  /** @ngInject */
  function fetchSvc($log, $http) {
    var api = 'https://hackerearth.0x10.info/api/one-push?type=json&query=list_websites';
    //var api = 'http://jwttester.getsandbox.com/contributors'
    var service = {
      getWebsites: getWebsites
    };

    return service;

    function getWebsites() {
      return $http.get(api)
        .then(getWebsitesComplete)
        .catch(getWebsitesFailed);

      function getWebsitesComplete(response) {
        return response.data;
      }

      function getWebsitesFailed(error) {
        $log.error('XHR Failed for get websites.\n' + angular.toJson(error.data, true));
      }
    }
  }
})();
