(function() {
  'use strict';

  angular
    .module('toppr')
    .controller('FetchController', FetchController);

  /** @ngInject */
  function FetchController($log, fetchSvc, $localStorage, toastr) {
    var vm = this;

    vm.like = function(website) {
      if(website.likes) {
        website.likes++;
      } else {
        website.likes = 1;
      }
      $localStorage.websites[website.id] = website.likes;
    }

    vm.clearLocalStorage = function () {
      $localStorage.clear();
    }
    activate();

    function activate() {
      getWebsites();
    }

    function getWebsites() {
      fetchSvc.getWebsites().then(function (data) {
          vm.websites = data.websites;
          toastr.success("Successfully fetched " + vm.websites.length + " websites");
          applyLocalStorage();

        }).catch(function (data) {
          $log.error(data);
        });
    }

    function applyLocalStorage() {
      if($localStorage.websites && Object.keys($localStorage.websites).length != 0) {
        toastr.warning("applied the local data");
        vm.websites.forEach(function(website){
          if($localStorage.websites.hasOwnProperty(website.id)) {
            website.likes = $localStorage.websites[website.id];
          }
        })
      } else {
        $localStorage.websites = {}
      }
    }


  }
})();
