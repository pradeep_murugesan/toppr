(function() {
  'use strict';

  angular
    .module('toppr')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/fetch/fetch.html',
        controller: 'FetchController',
        controllerAs: 'fetchCtrl'
      })
      .when('/push', {
        templateUrl: 'app/push/push.html',
        controller: 'PushController',
        controllerAs: 'pushCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
